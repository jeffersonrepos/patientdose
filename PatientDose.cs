using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Reflection;
using VMS.TPS.Common.Model.API;
using VMS.TPS.Common.Model.Types;
using System.Diagnostics;

using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;

using System.Windows;
using Application = VMS.TPS.Common.Model.API.Application;
using System.Drawing;

using Image = VMS.TPS.Common.Model.API.Image;


// TODO: Replace the following version attributes by creating AssemblyInfo.cs. You can do this in the properties of the Visual Studio project.
[assembly: AssemblyVersion("3.0.0.1")]
[assembly: AssemblyFileVersion("3.0.0.1")]
[assembly: AssemblyInformationalVersion("3.0")]

// TODO: Uncomment the following line if the script requires write access.
 [assembly: ESAPIScript(IsWriteable = true)]

namespace PatientDose
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                using (Application app = Application.CreateApplication())
                {
                    Execute(app);
                }
            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e.ToString());
            }
        }
        static void Execute(Application app)
        {
            Patient patient = app.OpenPatientById("04108898"); // 88315950
          
             patient.BeginModifications();  

            Course course = patient.Courses.Where(c => c.Id == "C1").Single();
            PlanSetup plan = course.PlanSetups.Where(c => c.Id == "VMAT1").Single();  //Plan1
            Study study = patient.Studies.Where(p => p.Id == "prost101613").First<Study>();
            Structure structure = plan.StructureSet.Structures.Where(c => c.Id == "RECTUM").Single(); //PTV5MM
            //Course>(patient.Courses).Items[0]).PlanSetups).Items[0]).Dose.Isodoses).Items[0]
            Dose dose = course.PlanSetups.Where(p => p.Id == "VMAT1").Single().Dose;   //(course.PlanSetups).Items[0]).Dose
                                                                                       //Isodose isodose = dose.GetVoxels(1,)
                                                                                       //var ourIDL = new Isodose;

            //foreach (Isodose isodose in dose.Isodoses)
            //{
            //    DoseValue doseValue = new DoseValue(6000, DoseValue.DoseUnit.cGy);
            //    structure.ConvertDoseLevelToStructure(dose, doseValue);
            //}
            var ss = course.PlanSetups.Where(c => c.Id == "VMAT1").Single().StructureSet;
            var isoStructure = ss.Structures.Where(s => s.Id == "zz95_ISO").FirstOrDefault();
            if (isoStructure == null)
            {

                var Iso95 = ss.AddStructure("DOSE_REGION", "zz95_ISO");  // second parameter is structure ID

                Iso95.ConvertDoseLevelToStructure(plan.Dose, new DoseValue(95.5, DoseValue.DoseUnit.Percent));
            }



            //DoseValue doseValue = new DoseValue(12.5, DoseValue.DoseUnit.Percent);
               //structure.ConvertDoseLevelToStructure(dose, doseValue);

            /* Example 1
             ss = planSetup.StructureSet;
             Iso95 = context.StructureSet.AddStructure("DOSE_REGION", "zz95_ISO");  // second parameter is structure ID
             Iso95.ConvertDoseLevelToStructure(context.PlanSetup.Dose, new DoseValue(95.5, DoseValue.DoseUnit.Percent));            

               Example 2  
               try drawing a structure using AddContourOnImagePlane.
               You feed the method a VVector array and a Z slice.
               AddContourOnImagePlane (VVector[] contour, int z))
              */


            String strtName = "";
            foreach (Structure strt in plan.StructureSet.Structures)
            {
                strtName = strtName + strt + "\n";
            }
           // System.Windows.Forms.MessageBox.Show(strtName, "StructureSet.Structures");

            //Studies
            String strModality = "";
            foreach (Series series in study.Series)
            {
                strModality = strModality + series.Modality + "\n";
            }

          //  System.Windows.Forms.MessageBox.Show(strModality, "patient.Study.Series");


            Image image = plan.StructureSet.Image;

            int[,] imagePlane = new int[image.XSize, image.YSize];


            //Mat converted = new Mat();
            //  String windowName = "Your Captcha"; //The name of the window
            //  CvInvoke.NamedWindow(windowName); //Create the window using the specific name
            // Mat<int>myMat = new Mat<int>(image.XSize, image.YSize,1);// DID Not work

            Matrix<double> fltr_img = new Matrix<double>( image.XSize, image.YSize);

            Mat finalColorImage = new Mat(image.XSize, image.YSize, DepthType.Cv8U, 3);

            Matrix<double> colorMappedImage = new Matrix<double>(image.XSize, image.YSize );  //normalized


            fltr_img.SetValue(0);
            var Origin = image.Origin;
            Debug.WriteLine("Origin= (" + Origin.x + Origin.y + Origin.z+")");
            Debug.WriteLine("image= ("  );

            for (int iSlice =70; iSlice < 80; iSlice++)
            {

             //   IEnumerable<Image> ImagesObj =  image.Series.Images;
              //  var imageslice= ImagesObj.Where(s => s.Id == "Image 84").FirstOrDefault();

                image.GetVoxels(iSlice, imagePlane);
                
                double max = 0;
                    for (int i = 0; i < image.XSize; i++)
                    {
                        for (int j = 0; j < image.YSize; j++)
                        {
                            fltr_img.Data[i, j] = imagePlane[i, j];
                        max = Math.Max(fltr_img.Data[i, j], max);
                        }

                    }

                // fltr_img.Mat.ConvertTo(converted, DepthType.Cv8U);

                
                CvInvoke.Normalize(fltr_img, colorMappedImage, 0, 255, Emgu.CV.CvEnum.NormType.MinMax, Emgu.CV.CvEnum.DepthType.Cv8U);
                CvInvoke.Imshow("image" + iSlice, colorMappedImage);  //Matrix implements the interface IInputarray


                //PRINTING THE STRUCTURES                

                foreach (Structure strt in plan.StructureSet.Structures)
                {
                                    
                    if (! String.Equals(strt.Id.ToString(),"zz95_ISO"))
                    {
                        continue;
                    }
                        
                    Debug.WriteLine("StructureID:" +strt.Id);
                    VVector[][] eclContour = strt.GetContoursOnImagePlane(iSlice);
                    Emgu.CV.Util.VectorOfVectorOfPoint contours = new Emgu.CV.Util.VectorOfVectorOfPoint();
                   
                    //contours.Push(new Emgu.CV.Util.VectorOfPoint(2));
                    Emgu.CV.Util.VectorOfPoint vp = new Emgu.CV.Util.VectorOfPoint(2);

                    for (int iContour = 0; iContour < eclContour.Length; iContour++)
                    {
                        Debug.WriteLine("iContour= " + iContour.ToString());

                        System.Drawing.Point[] ptArray = new System.Drawing.Point[eclContour[iContour].Length]; 
                        
                            for (int iPoint = 0; iPoint < eclContour[iContour].Length; iPoint++)
                                {
                                  //  Debug.WriteLine("\t iPoint= " + iContour.ToString() + " (x+Ox,y+Oy)   ---> (" +( eclContour[iContour][iPoint].x- Origin.x) + "," + (eclContour[iContour][iPoint].y+Origin.y)+")");

                                    Debug.WriteLine("\t iPoint= " + iPoint.ToString() + " (x,y,z)   ---> (" + (eclContour[iContour][iPoint].x ) + "," + (eclContour[iContour][iPoint].y)+")");
                                    // TODO : check size of image then cast to integ
                                    //Origin -The origin of the dose matrix. In other words, the DICOM coordinates of the center point of the upper-left hand corner voxel of the first dose plane.
                                    //X.Res  - The dose matrix resolution in X-direction in millimeters. 
                                    //(pt.x-ct.Origin.x)/ct.XRes
                                    // ptArray[iPoint] = new System.Drawing.Point((int) (eclContour[iContour][iPoint].y+255), (int)(eclContour[iContour][iPoint].x +255));
                           
                                    ptArray[iPoint] = new System.Drawing.Point((int)((eclContour[iContour][iPoint].y - Origin.y)/ image.YRes) , (int)((eclContour[iContour][iPoint].x - Origin.x) / image.XRes));

                                } // iPoint end

                        vp.Push(ptArray);
                      
                    } // iContour ends
                    contours.Push(vp);
                    //In order to show as color, colorMappedImage should be converted to RBG  https://stackoverflow.com/questions/58959488/cv2-drawcontours-isnt-displaying-correct-color
                    CvInvoke.CvtColor(colorMappedImage, finalColorImage, ColorConversion.Gray2Bgr);
                    //cvCvtColor.cvCvtColor(input, output, CV_GRAY2BGR);

                    CvInvoke.DrawContours(finalColorImage, contours, -1, new MCvScalar(255, 0, 0),thickness:2); 

                    CvInvoke.Imshow("image" + iSlice, finalColorImage);  //Matrix implements the interface IInputarray

                    

                    

                    CvInvoke.WaitKey(0);

                        // CvInvoke.FindContours(colorMappedImage, contours, null, RetrType.External, ChainApproxMethod.ChainApproxSimple);

                   
                    break;

                } //strt ends

            } // iSlice ends

            app.SaveModifications();
            //Generate DVH

            app.ClosePatient();

        }


    }

}
